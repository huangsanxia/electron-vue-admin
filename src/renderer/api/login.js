
import request from '@/utils/request'
export function login(username, password) {
  return request({
    url: '/login',
    method: 'post',
    data: {
      username,
      password
    }
  })
}

export function getInfo(token) {
  return new Promise(function(resolve) {
    setTimeout(function() {
      resolve({
        'code': 20000,
        'data': {
          roles: ['ener'],
          name: 'luhang',
          avatar: ''
        }
      })
    }, 500)
  })
}

export function logout() {
  return new Promise(function(resolve) {
    setTimeout(function() {
      resolve({
        'code': 20000,
        'data': {
        }
      })
    }, 500)
  })
}

export function getDicture(data = {}) {
  return request({
    url: '/dict/find-by-dict-names',
    method: 'post',
    data
  })
}

export function queryAllInfo(data = {}) {
  return request({
    url: '/declare/elements/query-all',
    method: 'post',
    data
  })
}
