const Store = require('electron-store')
const store = new Store()
var config = {}
config.taskInterval = 'Path-Key'
config.getTaskInterval = () => {
  return store.get(config.taskInterval) || 1
}
config.setTaskInterval = (val) => {
  store.set(config.taskInterval, val)
}

export { config }
