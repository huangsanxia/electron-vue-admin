import Cookies from 'js-cookie'
import { config } from './config'
const TokenKey = 'Admin-Token'

export function getToken() {
  return Cookies.get(TokenKey)
}

export function setToken(token) {
  return Cookies.set(TokenKey, token)
}

export function removeToken() {
  return Cookies.remove(TokenKey)
}

export function setPath(path) {
  config.setTaskInterval(path)
  return path
}

export function getPath() {
  return config.getTaskInterval() === 1 ? 'D:/' : config.getTaskInterval()
}
