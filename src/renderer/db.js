import Datastore from 'nedb'
//  import path from 'path'
//  import { remote } from 'electron'

//  const baseUrl = 'D:\\'

const db = {
  DECLARE_DICT_1: new Datastore({ filename: 'dict1.json', autoload: true }),
  DECLARE_DICT_2: new Datastore({ filename: 'dict2.json', autoload: true }),
  DECLARE_DICT_3: new Datastore({ filename: 'dict3.json', autoload: true }),
  DECLARE_DICT_102: new Datastore({ filename: 'dict102.json', autoload: true }),
  DECLARE_DICT_103: new Datastore({ filename: 'dict103.json', autoload: true }),
  DECLARE_DICT_105: new Datastore({ filename: 'dict105.json', autoload: true }),
  goodsData: new Datastore({ filename: 'goods.json', autoload: true }),
  companyData: new Datastore({ filename: 'company.json', autoload: true })
}

export default {
  db
}
